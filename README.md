Кроссплатформенная компиляция в linux с использованием docker

1 Запустить docker и каталога в котором находится main.go

```bash
$:~ docker run --rm -it -v "$PWD":/usr/src/goproxyme -w /usr/src/goproxyme -e GOOS= -e GOARCH= golang:1.5.1 bash
```

2 Выполнить компиляцию под все платформы

```bash
for GOOS in darwin linux windows; do
   for GOARCH in 386 amd64; do
     go build -v -o goresolution-$GOOS-$GOARCH
   done
done
```
