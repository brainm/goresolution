package main

import (
	"os"
	"path/filepath"
	"log"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"flag"
	//"strings"
	"strings"
	"fmt"
	"io"
)

var (
	dim map[string][]string
)

func init(){
	dim = map[string][]string{
		"320x240":[]string{},
		"640x480":[]string{},
		"800x600":[]string{},
		"1024x768":[]string{},
		"1600x1200":[]string{},

		"240x320":[]string{},
		"480x640":[]string{},
		"600x800":[]string{},
		"768x1024":[]string{},
		"1200x1600":[]string{},
	}
}

func copyFile(src, dst string) (err error) {
	sfi, err := os.Stat(src)
	if err != nil {
		return
	}
	if !sfi.Mode().IsRegular() {
		// cannot copy non-regular files (e.g., directories,
		// symlinks, devices, etc.)
		return fmt.Errorf("CopyFile: non-regular source file %s (%q)", sfi.Name(), sfi.Mode().String())
	}
	dfi, err := os.Stat(dst)
	if err != nil {
		if !os.IsNotExist(err) {
			return
		}
	} else {
		if !(dfi.Mode().IsRegular()) {
			return fmt.Errorf("CopyFile: non-regular destination file %s (%q)", dfi.Name(), dfi.Mode().String())
		}
		if os.SameFile(sfi, dfi) {
			return
		}
	}
	if err = os.Link(src, dst); err == nil {
		return
	}
	err = copyFileContents(src, dst)
	return
}

func copyFileContents(src, dst string) (err error) {
	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()
	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer func() {
		cerr := out.Close()
		if err == nil {
			err = cerr
		}
	}()
	if _, err = io.Copy(out, in); err != nil {
		return
	}
	err = out.Sync()
	return
}

func getImageDimension(imagePath string) (int, int, error) {
	file, err := os.Open(imagePath)
	if err != nil {
		return 0,0, err
	}

	image, _, err := image.DecodeConfig(file)
	if err != nil {
		return 0,0, err
	}
	return image.Width, image.Height, err
}

func walker(path string, f os.FileInfo, err error) (e error) {
	if filepath.Ext(f.Name()) == ".png" || filepath.Ext(f.Name()) == ".jpg" || filepath.Ext(f.Name()) == ".jpeg"{
		w, h, err := getImageDimension(path)
		if err == nil {
			if h <= w {
				if w < 320 && h < 240 {
					// = 320x240
					dim["320x240"] = append(dim["320x240"], path)
				}else if w < 800 && h < 600 {
					// = 640x480
					dim["640x480"] = append(dim["640x480"], path)
				}else if w < 1024 && h < 768 {
					// = 800x600
					dim["800x600"] = append(dim["800x600"], path)
				}else if w < 1600 && h < 1200 {
					// = 1024x768
					dim["1024x768"] = append(dim["1024x768"], path)
				}else{
					// = 1600x1200
					dim["1600x1200"] = append(dim["1600x1200"], path)
				}
			}else{
				if w < 240 && h < 320 {
					// = 240x320
					dim["240x320"] = append(dim["240x320"], path)
				}else if w < 600 && h < 800 {
					// = 480x640
					dim["480x640"] = append(dim["480x640"], path)
				}else if w < 768 && h < 1024 {
					// = 600x800
					dim["600x800"] = append(dim["600x800"], path)
				}else if w < 1200 && h < 1600 {
					// = 768x1024
					dim["768x1024"] = append(dim["768x1024"], path)
				}else{
					// = 1200x1600
					dim["1200x1600"] = append(dim["1200x1600"], path)
				}
			}
		}else{
			log.Printf("err: %v", err)
		}
	}
	return
}

func main() {
	_in := flag.String("in", "in", "input directory")
	_out := flag.String("out", "out", "output directory")
	verbose := flag.Bool("verbose", false, "verbose")
	flag.Parse()
	//
	fmt.Printf("Start application\n")
	in := strings.TrimSuffix(*_in, string(filepath.Separator))
	fmt.Printf("Read input: %v\n", in)
	out := strings.TrimSuffix(*_out, string(filepath.Separator))
	fmt.Printf("Save output: %v\n", out)
	//
	filepath.Walk(in, walker)
	for k, paths := range dim {
		if len(paths) > 0 {
			fmt.Printf("[%v]: %d\n", k, len(paths))
			for _, in_path := range paths {
				_path := strings.Split(out + string(filepath.Separator) + k + in_path[len(in):len(in_path)], string(filepath.Separator))
				os.MkdirAll(strings.Join(_path[0:len(_path) - 1], string(filepath.Separator)), 0777);
				out_path := out + string(filepath.Separator) + k + in_path[len(in):len(in_path)]
				copyFile(in_path, out_path)
				if *verbose {
					fmt.Printf("\t%v => %v\n", in_path, out_path)
				}
			}
		}
	}
}


